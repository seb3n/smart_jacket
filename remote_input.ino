//Author: seben
//Description: This script is used in the first prototype of the Smart Jacket, which uses remote inputs that simulate the usr turning on his //headlights, brake lights turning signals or hazard lights using the switch on his vehicle.
//--------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------

#include <Adafruit_NeoPixel.h>

#define PIN 13

Adafruit_NeoPixel strip = Adafruit_NeoPixel( 60, PIN, NEO_GRB + NEO_KHZ800);

// Remote Location at Initial Condition.
int Button_A = A2;
int A_Value = 0;

int Button_B = A3;
int B_Value = 0;

int Button_C = A4;
int C_Value = 0;

int Button_D = A5;
int D_Value = 0;
//------------------------------------------------------

// Led Location.
int yellow = 6;
int red = 7; 
int blue = 8; 
int white = 9;
//-------------------------------------------------------

//Neo_Pixel color functions
// define the color magenta as a function
uint32_t magenta = strip.Color( 255, 0, 255);
//define green color
uint32_t green = strip.Color( 0, 255, 0);
// define blue color
uint32_t bleu = strip.Color( 0, 0, 255);
// define red color 
uint32_t rouge = strip.Color( 255, 0, 0);
//define medium red
uint32_t rouge_pale = strip.Color( 60, 0, 0);
//define off color
uint32_t off = strip.Color( 0, 0, 0);
//define white color
uint32_t blanc = strip.Color( 255, 255, 255);
//define pink color
uint32_t pink = strip.Color(255, 0, 100);
//define orange color
uint32_t orange = strip.Color(255, 63, 0);
//---------------------------------------------------------------

//---------------------------------------------------------------
//---------------------------------------------------------------
//---------------------------------------------------------------
// Setup.
void setup()
{
  strip.begin();
  strip.show(); //initialize pixels to off
  
  //Set inputs and outputs
  pinMode(yellow,OUTPUT);
  pinMode(red, OUTPUT);
  pinMode(blue, OUTPUT);
  pinMode(white, OUTPUT);
  pinMode(Button_A, INPUT);
  pinMode(Button_B, INPUT);
  pinMode(Button_C, INPUT);
  pinMode(Button_D, INPUT);
  
  //Display values
  Serial.begin(9600);
  Serial.println(" Initialisation Complete ");
  delay(3);
  LightsCheck();
  Serial.println(" Lights Test Complete ");
  delay(3);
  Serial.println(A_Value);
  delay(3);
  Serial.println(B_Value);
  delay(3);
  Serial.println(C_Value);
  delay(3);
  Serial.println(D_Value);
  delay(3);
  Serial.println("/");
  delay(3);
}
//--------------------------------------------------------------

//--------------------------------------------------------------
//--------------------------- Loop -----------------------------
//--------------------------------------------------------------
void loop()
{
//Read remote values.
  int A_Value = analogRead(Button_A);
  delay(3);
  int B_Value = analogRead(Button_B);
  delay(3);
  int C_Value = analogRead(Button_C);
  delay(3);
  int D_Value = analogRead(Button_D);
  delay(3);
  Serial.println(A_Value);
  delay(3);
  Serial.println(B_Value);
  delay(3);
  Serial.println(C_Value);
  delay(3);
  Serial.println(D_Value);
  delay(3);
//---------------------------------------------------------------

//Headlights on.
 while(A_Value == 1023)
 {
   HeadLight_on();
 
   if(B_Value == 1023)
   {
     BrakeLight_on();
   }
   if(C_Value == 1023 && D_Value == 0)
   {
     LeftSignal_on();
   }
   if(C_Value == 0 && D_Value == 1023)
   {
     RightSignal_on();
   }
   if(C_Value == 1023 && D_Value == 1023)
   {
     Hazard();
   }
   
// Read remote values to reinitializse loop if necesary.
   A_Value = analogRead(Button_A);
  delay(3);
   B_Value = analogRead(Button_B);
  delay(3);
   C_Value = analogRead(Button_C);
  delay(3);
   D_Value = analogRead(Button_D);
  delay(3);
 }

//---------------------------------------------------------------

// Headlights off.
  if(A_Value == 0)
 {
   HeadLight_off();
   //delay(3);
 }
//---------------------------------------------------------------

//Brake light on. 
 while(B_Value == 1023)
 {
   BrakeLight_on();
   
//   if(A_Value == 0)
//   {
//     HeadLight_off();
//   }
   if(A_Value == 1023)
   {
     HeadLight_on();   // <--- stays on until brake is off, if not then the delay is too long. (minor detail, could turn into signature)
   }
   if(C_Value == 1023 && D_Value == 0)
   {
     LeftSignal_on();
   }
   if(C_Value == 0 && D_Value == 1023)
   {
     RightSignal_on();
   }
   if(C_Value == 1023 && D_Value == 1023)
   {
     Hazard();
   }
   
// Read remote values to reinitializse loop if necesary.
   A_Value = analogRead(Button_A);
  //delay(3);
   B_Value = analogRead(Button_B);
  //delay(3);
   C_Value = analogRead(Button_C);
  //delay(3);
   D_Value = analogRead(Button_D);
  //delay(3);
 }
//--------------------------------------------------------------

//Brake light off. 
// if(B_Value == 0 && A_Value == 0)
// {
//   BrakeLight_off();
//   delay(3);
// }
//--------------------------------------------------------------
 
//Turn signals on.
 while(C_Value == 1023)
 {
   if(C_Value == 1023 && D_Value == 0)
   {
     LeftSignal_on();
   }   
   if(A_Value == 1023)
   {
     HeadLight_on();
   }
   if(B_Value == 1023)
   {
     BrakeLight_on();
   }
   if(D_Value == 1023)
   {
     Hazard();
   }
// Read remote values to reinitialize loop if necesary.
   A_Value = analogRead(Button_A);
  delay(3);
   B_Value = analogRead(Button_B);
  delay(3);
   C_Value = analogRead(Button_C);
  delay(3);
   D_Value = analogRead(Button_D);
  delay(3);
 }
 
 while(D_Value == 1023)
 {
   if(C_Value == 0 && D_Value == 1023)
   {
     RightSignal_on();
   }   
   if(A_Value == 1023)
   {
     HeadLight_on();
   }
   if(B_Value == 1023)
   {
     BrakeLight_on();
   }
   if(C_Value == 1023)
   {
     Hazard();
   }
// Read remote values to reinitializse loop if necesary.
    A_Value = analogRead(Button_A);
  delay(3);
   B_Value = analogRead(Button_B);
  delay(3);
   C_Value = analogRead(Button_C);
  delay(3);
   D_Value = analogRead(Button_D);
  delay(3);
 }
//--------------------------------------------------------------

// Turn Hazard on.
 while(C_Value == 1023 && D_Value == 1023)
 {
   Hazard();
   
   if(A_Value == 1023)
   {
     HeadLight_on();
   }
   if(B_Value == 1023)
   {
     BrakeLight_on;
   }
// Read remote values to reinitializse loop if necesary.
    A_Value = analogRead(Button_A);
  delay(3);
   B_Value = analogRead(Button_B);
  delay(3);
   C_Value = analogRead(Button_C);
  delay(3);
   D_Value = analogRead(Button_D);
  delay(3);
 }
//--------------------------------------------------------------

//Turn signals off. 
// if(C_Value == 0)
// {
//   LeftSignal_off();
// }
 
// if(D_Value == 0)
// {
//   RightSignal_off();
// } 
//---------------------------------------------------------------

//Get & save GPS location.---------
//Check rainsensor.--------- != capacitive touch------ do nxt.
//Check users pulse.---------
//Check user orientation---------(9_DOF)
//Check user position---------(9_DOF)
//Check impact sensors--------- (9_DOF)
//Analize data.---------
//Take necessary action.---------
//---------------------------------------------------------------
}

//---------------------------------------------------------------
//---------------------------------------------------------------
//---------------------------------------------------------------
//---------------------------------------------------------------
//---------------------------------------------------------------
//---------------------------------------------------------------

//Lights Check.
void LightsCheck()
{
  digitalWrite( white, HIGH);
  delay(300);
  digitalWrite(white, LOW);
  delay(300);
  digitalWrite(yellow, HIGH);
  delay(300);
  digitalWrite(yellow, LOW);
  delay(300);
  digitalWrite(blue, HIGH);
  delay(300);
  digitalWrite(blue, LOW);
  delay(300);
  digitalWrite(red, HIGH);
  delay(300);
  digitalWrite(red, LOW);
  delay(500); 
  digitalWrite(white, HIGH);
  digitalWrite(red, HIGH);
  digitalWrite(yellow, HIGH);
  digitalWrite(blue, HIGH);
  delay(300);
  digitalWrite(white, LOW);
  digitalWrite(red, LOW);
  digitalWrite(yellow, LOW);
  digitalWrite(blue, LOW);
  colorWipe(rouge, 50);
  colorWipe(off, 50);
  colorWipe(orange, 50);
  colorWipe(off, 50);

}
//-------------------------------------------------------------

// Output Functions.

void HeadLight_on()
{
  digitalWrite(white, HIGH);
//Red led at medium intensity .
  digitalWrite(red, HIGH);
  colorWipe(rouge_pale,1);
  //delay(100);
}

void HeadLight_off()
{
  digitalWrite(white, LOW);
  digitalWrite(red, LOW);
  colorWipe(off,0);
  //delay(300);
}

void BrakeLight_on()
{
//Red light blinking at maximum intensity.
  digitalWrite(red, HIGH);
  colorWipe(rouge,3);
  //delay(100);
  digitalWrite(red, LOW);
  colorWipe(off,3); //                                <-----------------
  //delay(100);
}

//void BrakeLight_off()
//{
//  digitalWrite(red,LOW);
//}

void LeftSignal_on()
{
  digitalWrite(yellow, HIGH);
  colorWipe(orange, 3);
  delay(200);
  digitalWrite(yellow, LOW);
  colorWipe(off, 3);
  delay(200);
}

//void LeftSignal_off()
//{
//  digitalWrite(orange, LOW);  <-------
//}

void RightSignal_on()
{
  digitalWrite(blue, HIGH);
  colorWipe(orange, 3);
  delay(200);
  digitalWrite(blue, LOW);
  colorWipe(off,3);
  delay(200);
}

//void RightSignal_off()
//{
//  digitalWrite(blue, LOW);  <--------
//}

void Hazard()
{
  digitalWrite(yellow, HIGH);
  digitalWrite(blue, HIGH);
  colorWipe(orange,3);
  delay(300);
  digitalWrite(yellow, LOW);
  digitalWrite(blue, LOW);
  colorWipe(off,3);
  delay(300);
}

void colorWipe(uint32_t c, uint8_t wait)
{
  for(uint16_t i=0; i<strip.numPixels(); i++)
  {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}

void BrakeLeft()
{
  digitalWrite(red, HIGH); //Should make blink at faster rate than signal.
  digitalWrite(yellow, HIGH);
  delay(300);
  digitalWrite(yellow, LOW);
  delay(100);
  digitalWrite(red, LOW);
  delay(200);
}

// void ReadRemoteValues()            <-----------
//{
 //  A_Value = analogRead(Button_A);
 // delay(3);
 //  B_Value = analogRead(Button_B);
 // delay(3);
 //  C_Value = analogRead(Button_C);
 // delay(3);
 //  D_Value = analogRead(Button_D);
 // delay(3);
//}
//--------------------------------------------------------------

