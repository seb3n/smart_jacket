# README #

This code was written as the first prototype for a smart jacket that would help increase the visibility of bicycle or motorcycle riders. This smart jacket would respond to wireless user inputs when he turns his lights on, whether they are turning signals, hazards, headlights or tail lights. A capacitive sensors is also used to activate signals when water is detected on the jacket, indicating rain and a reduction in visibility. A GPS module, gyroscope, accelerometer, impact sensor and heart rate monitoring sensors are also envisioned for this jacket in order to provide a rider monitoring system and emergency response in the case of a collision.

remote_input.ino deals with the user input of the light signals.

water_sensor_input.ino deals with detecting rain and activating the light signals.