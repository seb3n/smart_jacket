//Author: Seben
//Description: This script is used in the first prototype of the Smart Jacket as an addition to 'Remote_inut.ino'. It is used to activate light signals //with usr input through a remote or a capacitive sensor made with a conductive fabric. The signals can also be activated when water is detected //through the capacitive sensor. An Adafruit Neopixel LED strip is used as light signal.
//-------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------- 

#include <Adafruit_NeoPixel.h>
#include <CapacitiveSensor.h>

// define remote input
//define remote initial values
//define LEDs
//define NEO_PIXEL values

#define PIN 13
Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, PIN, NEO_GRB + NEO_KHZ800);
int remoteA  = A2;
int remoteB  = A3;
int remoteC  = A4;
int remoteD  = A5;

int remoteValueA = 0;
int remoteValueB = 0;
int remoteValueC = 0;
int remoteValueD = 0;

int yellowLed = 6;
int redLed   = 7;
int blueLed  = 8;
int whiteLed = 9;

CapacitiveSensor cs_3_4 = CapacitiveSensor(3,4);

// define colors as functions
  uint32_t magenta = strip.Color(255, 0, 255);
  uint32_t green = strip.Color(0, 255, 0);
  uint32_t blue = strip.Color(0, 0, 255);
  uint32_t red = strip.Color(255, 0, 0);
  uint32_t off = strip.Color(0, 0, 0);
  uint32_t white = strip.Color(255, 255, 255);
  uint32_t pink = strip.Color(255, 0, 100);
  uint32_t orange = strip.Color(255, 63, 0);
  
void setup()
{
  //startup the LED strip
  strip.begin();
  //update the strip, to start they are all "off"
  strip.show();
  
  cs_3_4.set_CS_AutocaL_Millis(0XFFFFFFFF); //turn off auto calibration
  Serial.begin(9600);
  //declare the LED pins as output

 // pinMode(yellowLed, OUTPUT);
 // pinMode(redLed,   OUTPUT);
 // pinMode(blueLed,  OUTPUT);
 // pinMode(whiteLed, OUTPUT);
 // pinMode(remoteA, INPUT);
 // pinMode(remoteB, INPUT);
 // pinMode(remoteC, INPUT);
 // pinMode(remoteD, INPUT);
  
}

void loop()
{
  
   remoteValueA = analogRead(remoteA);
   remoteValueB = analogRead(remoteB);
   remoteValueC = analogRead(remoteC);
   remoteValueD = analogRead(remoteD); 
  
  //Alertness();
  Serial.println(remoteValueA);
  Serial.println(remoteValueB);
  Serial.println(remoteValueC);
  Serial.println(remoteValueD);
  
  long start = millis();
  long total1 = cs_3_4.capacitiveSensor(30); //value output of the capacitive sensor
  
  if(total1 > 4400)
  {
    GoPink(pink, 50);
    delay(300);
    GoOrange(orange,50);
    delay(300);
    hideAll();
    delay(300);
    
   // digitalWrite(yellowLed, HIGH);
   // digitalWrite(blueLed, HIGH);
   // digitalWrite(whiteLed, HIGH);
   // digitalWrite(redLed, HIGH);
  }
  else
  {
    Alertness();
  }
  
  Serial.print(millis() - start);
  Serial.print("\t");
  
  Serial.print(total1); 
  delay(10);
}

void Alertness()
{
  if(remoteValueC == 1023) //double check with serial monitor
  {
    GoWhite(white, 50);
    delay(300);
    hideAll();
    delay(300);
  }
  else
  {
    hideAll();
  }
  
  if( remoteValueA == 1023)
  {
   GoGreen(green, 50);
   delay(300);
   hideAll();
   delay(300);
  }
  else
  {
   hideAll();
  }
  
   if( remoteValueB == 1023)
  {
   GoBlue(blue, 50);
   delay(300);
   hideAll();
  }
  else
  {
   hideAll();
  }
  
   if( remoteValueD == 1023)
  {
    GoRed(red, 50);
    delay(300);
    hideAll();
    delay(300);
  }
  else
  {
   hideAll();
  }
}

void GoPink(uint32_t c, uint8_t wait)
{
  for( uint16_t i=0; i<strip.numPixels(); i++)
  {
    strip.setPixelColor(i,c);
    strip.setBrightness(150);
    strip.show();
    delay(wait);
  }
}

void hideAll()
{
   for( uint16_t i=0; i<strip.numPixels(); i= i++)
   {
     strip.setPixelColor(i, off);
     strip.setBrightness(64);
     strip.show();
     delay(25);
   }
}

void GoWhite(uint32_t c, uint8_t wait)
{
  for( uint16_t i=0; i<strip.numPixels(); i++)
  {
    strip.setPixelColor(i,c);
    strip.setBrightness(150);
    strip.show();
    delay(wait);
  }
}

void GoGreen(uint32_t c, uint8_t wait)
{
  for(uint16_t i=0; i<strip.numPixels(); i++)
  {
   strip.setPixelColor(i,c);
   strip.setBrightness(150);
   strip.show();
   delay(wait);
  }
}

void GoBlue(uint32_t c, uint8_t wait)
{
  for(uint16_t i=0; i<strip.numPixels(); i++)
  {
    strip.setPixelColor(i,c);
   strip.setBrightness(150);
   strip.show();
   delay(wait);
  }
}

void GoRed(uint32_t c, uint8_t wait)
{
  for(uint16_t i=0; i<strip.numPixels(); i++)
  {
    strip.setPixelColor(i,c);
   strip.setBrightness(150);
   strip.show();
   delay(wait);
  }
}

void GoOrange(uint32_t c, uint8_t wait)
{
  for(uint16_t i=0;  i<strip.numPixels(); i++)
  {
    
   strip.setPixelColor(i,c);
   strip.setBrightness(150);
   strip.show();
   delay(wait);
  }
}

